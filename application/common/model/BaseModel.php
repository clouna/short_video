<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.csh.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Model;

class BaseModel extends Model
{

    /**
     * 获取详情
     * @param array $where
     * @return array
     */
    public function getOne($where=[])
    {
        return $this->where($where)->find();
    }

    /**
     * 分页列表
     * @param $where array 筛选
     * @param $order string 排序
     * @param $pageNum int 每页行数
     * @return array
     */
    public function getPageList($where=[], $order='id', $pageNum = 7)
    {
        return $this->where($where)->order($order)->paginate($pageNum);
    }

    /**
     * 设置信息
     * @param $param
     * @return array
     */
    public function modify($param)
    {
        try{
            $this->allowField(true)->save($param, $param['id']?['id'=>$param['id']]:[]);
            return msg(1, '', $param['id']?'修改成功！':'发布成功！');
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 删除信息
     * @param $id
     * @return array
     */
    public function remove($id)
    {
        try{
            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');
        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }
}