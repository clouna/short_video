<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Model;

class NoticeModel extends Model
{
    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_notice';
        parent::__construct($data);
    }

    /**
     * 返回原有数据  不自动进行时间转换
     * @param $time
     * @return mixed
     */
    public function getCreateTimeAttr($time)
    {
        return $time;
    }

    /**
     * 咨询列表
     * @param $where array 筛选
     * @param $order string 排序
     * @param $pageNum int 每页行数
     * @return array
     */
    public function getNoticePage($where=[], $order='id', $pageNum = 7)
    {
        return $this->where($where)->order($order)->paginate($pageNum);
    }

    /**
     * 添加公告
     * @param $param
     * @return array
     */
    public function insertNotice($param)
    {
        try{
            $result = $this->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{
                return msg(1, url('user/index'), '添加公告成功');
            }
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 获取单个咨询
     * @param array $where
     * @return array
     */
    public function getOneNotice($where=[])
    {
        return $this->where($where)->find();
    }

    /**
     * 编辑咨询
     * @param $param
     * @return array
     */
    public function editNotice($param)
    {
        try{
            $this->allowField(true)->save($param, $param['id']?['id'=>$param['id']]:[]);
            return msg(1, '', $param['id']?'修改成功！':'发布成功！');
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 删除咨询
     * @param $id
     * @return array
     */
    public function delNotice($id)
    {
        try{
            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');
        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }
}