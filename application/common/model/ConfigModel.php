<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.csh.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Model;

class ConfigModel extends BaseModel
{
    /**
     * 构造方法
     */
    public function __construct(){
        parent::__construct('config');
    }

    /**
     * 更新配置
     * @param string $key 键
     * @param string $value 值
     * @param string $default 默认值
     * @return array 配置
     */
    public function full_id($key, $value, $default=''){
        $data = [
            'key'=>$key,
            'value'=>is_null($value) ? $default : $value,
        ];
        $config = self::get(['key'=>$key]);
        $id = $config['id']?:0;
        if ($id){
            $data['id'] = $id;
        }
        return $data;
    }

    /**
     * 读取配置
     * @param string $key 键
     * @param string $default 默认值
     * @return string 值
     */
    public static function get_value($key, $default=''){
        $data = self::get(['key'=>$key]);
        return $data && $data['value'] ? $data['value'] : $default;
    }
}