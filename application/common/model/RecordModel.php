<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Model;

class RecordModel extends Model
{

    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_record';
        parent::__construct($data);
    }

    /**
     * 返回原有数据  不自动进行时间转换
     * @param $time
     * @return mixed
     */
    public function getCreateTimeAttr($time)
    {
        return $time;
    }
    /**
     * 腹透列表
     * @param $where array 筛选
     * @param $order string 排序
     * @param $pageNum int 每页行数
     * @return array
     */
    public function getRecordPage($where=[], $order='id', $pageNum = 10)
    {
        return $this->where($where)->order($order)->paginate($pageNum);
    }

    /**
     * 根据搜索条件获取所有的公告
     * @param $where
     * @return array
     */
    public function getRecordByWhere($where=[])
    {
        return $this->where($where)->order('create_time desc')->paginate(7);
    }

    /**
     * 查看今日腹透记录次数
     * @param $id int 会员id
     * @return int
     */
    public function getRecordCountToday($id)
    {
        return $this->where('to_days(create_time) = to_days(now()) and mid='.$id)->count();
    }

    /**
     * 添加公告
     * @param $param
     * @return array
     */
    public function addRecord($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if($result){
                return msg(1, '', '添加记录成功');
            }else{
                return msg(1, '', '添加记录失败');
            }
        }catch(\Exception $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 删除公告
     * @param $id
     * @return array
     */
    public function delRecord($id)
    {
        try{
            $this->where('id', $id)->delete();
            return msg(1, '', '删除公告成功');
        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }
}