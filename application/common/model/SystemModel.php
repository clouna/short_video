<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.csh.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Model;

class SystemModel extends Model
{
    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_system';
        parent::__construct($data);
    }

    /**
     * 获取微信设置
     * @param array $where
     * @return array
     */
    public function getSystem($where=[])
    {
        return $this->where($where)->find();
    }

    /**
     * 编辑微信设置
     * @param $param
     * @return array
     */
    public function editSystem($param)
    {
        try{
            $this->allowField(true)->save($param);
            return msg(1, '', '保存成功！');
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }
}