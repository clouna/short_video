<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\common\model;

use think\Exception;
use think\Model;

class MemberModel extends Model
{
    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_member';
        parent::__construct($data);
    }

    /**
     * 返回原有数据  不自动进行时间转换
     * @param $time
     * @return mixed
     */
    public function getCreateTimeAttr($time)
    {
        return $time;
    }

    /**
     * 注册新用户
     * @param $param
     * @return array
     */
    public function insertMember($param)
    {
        try{
            $result = $this->validate('MemberValidate')->save($param);
            if(false == $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{
                return msg(1, $this->data, '添加记录成功');
            }
        }catch(\Exception $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 注册新用户
     * @param $param
     * @return array
     */
//    public function inMember($param)
//    {
//        try{
//            $this->startTrans();
//            $account = new AccountModel();
//            $res1 = $this->save($param);
//            $data['mid'] = $this->id;
//            $data['password'] = md5(substr($param['phone'], -6));
//            $res2 = $account->insert($data);
//            if($res1 && $res2){
//                $this->commit();
//                return msg(1, '', '注册成功');
//            }else{
//                $this->rollback();
//                return msg(-3, '', '注册失败');
//            }
//        }catch(Exception $e){
//            $this->rollback();
//            return msg(-1, '', $e->getMessage());
//        }
//    }

    /**
     * 患者列表
     * @param $where array 筛选
     * @param $order string 排序
     * @param $pageNum int 每页行数
     * @return array
     */
    public function getMemberPage($where=[], $order='id', $pageNum = 7)
    {
        return $this->where($where)->order($order)->paginate($pageNum);
    }

    /**
     * 根据当前患者资料
     * @param $where array 筛选
     * @param $column string 属性
     * @return array
     */
    public function getMemberInfo($where=[], $column='*')
    {
        return $this->field($column)->where($where)->find();
    }

    /**
     * 根据手机号查找用户
     * @param $name
     * @return Model
     */
    public function findUserByPhone($name)
    {
        return $this->where('phone', $name)->find();
    }

    /**
     * 修改用户信息
     * @param array $param
     * @param $uid
     * @return array
     */
    public function editMember($param = [])
    {
        try{
            $this->allowField(true)->save($param, $param['id']?['id'=>$param['id']]:[]);
            return msg(1, '', '修改成功！');
        }catch (\Exception $e){
            return msg(-1, '', $e->getMessage());
        }
    }
}