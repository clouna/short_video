<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    'login' => 'index/index/login',//登录
    'level' => 'index/index/level',//登录
    'info' => 'index/member/info',//获取会员信息
    'update' => 'index/member/update',//修改会员信息
    'record' => 'index/record/upload',//上传腹透记录
    'noticeList' => 'index/notice/noticeList',//资讯列表
    'noticeDetail' => 'index/notice/noticeDetail',//资讯详情
    'notice' => 'index/notice/detail',//资讯详情页
    'doctorList' => 'index/doctor/doctorList',//医生列表
    'doctorDetail' => 'index/doctor/doctorDetail',//医生详情
    'doctor' => 'index/doctor/detail',//医生详情页
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],
];