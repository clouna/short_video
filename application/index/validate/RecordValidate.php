<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\index\validate;

use think\Validate;

class RecordValidate extends Validate
{
    protected $rule = [
        ['gxy', 'require|float|between:0,250', '高血压不能为空|高血压格式不对|高血压超出范围'],
        ['dxy', 'require|float|between:0,250', '低血压不能为空|低血压格式不对|低血压超出范围'],
        ['weight', 'require|float|between:35,150', '体重不能为空|体重格式不对|体重超出范围'],
        ['level', 'require|integer|between:1,2', '舒适度不能为空|舒适度格式不对|舒适度超出范围'],
        ['hy_time', 'datetime', '换液时间格式不对'],
        ['nd', 'require|float|min:0', '腹透液浓度不能为空|腹透液浓度格式不对|腹透液浓度超出范围'],
        ['grl', 'require|float|min:0', '灌入量不能为空|灌入量格式不对|灌入量超出范围'],
        ['yll', 'require|float|min:0', '引流量不能为空|引流量格式不对|引流量超出范围'],
        ['cll', 'float|min:0', '超滤量格式不对|超滤量超出范围'],
        ['nl', 'require|float|min:0', '尿量不能为空|尿量格式不对|尿量超出范围'],
        ['ysl', 'require|float|min:0', '饮水量不能为空|饮水量格式不对|饮水量超出范围']
    ];

}