<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\index\validate;

use think\Validate;

class MemberValidate extends Validate
{
    protected $rule = [
        ['name', 'chs', '请填写中文'],
        ['age', 'number|between:18,99', '年龄格式不正确|年龄范围不对'],
        ['phone', 'number|length:7,13', '手机号格式不正确|手机号长度不正确'],
        ['email', 'email', '邮箱格式不正确'],
        ['number', 'int|min:1', '腹透次数格式不正确|腹透次数超出范围']
    ];
}