<?php
/**
 * Created by PhpStorm.
 * User: Clouna
 * Date: 2018/12/14
 * Time: 12:34
 */

namespace app\index\controller;

use app\common\model\DoctorModel;
use think\Controller;

class Doctor extends Controller
{
    // 医生列表
    public function doctorList()
    {
        $pageNumber = input("post.pageNumber")?:1;
        $pageCount = input("post.pageCount")?:10;

        $doctor = new DoctorModel();
        $list = $doctor->field('id,name,logo,skill,create_time')
            ->where('status<>9')->order('create_time desc')
            ->limit(($pageNumber-1)*$pageCount, $pageCount)->select();
        $total = $doctor->where('status<>9')->count();

        return json([
            'code' => 1,
            'data' => $list,
            'total' => $total,
            'domain' => config('domain') . '/public/uploads/',
            'msg' => '医生列表'
        ]);
    }

    // 医生详情
    public function doctorDetail()
    {
        $id = input('param.id');
        if(empty($id)){
            return json(['code'=>2, 'msg'=>'参数错误']);
        }

        $doctor = new DoctorModel();
        $doctor = $doctor->getOneDoctor('status<>9 and id='.$id);
        if(empty($doctor)){
            return json(['code'=>2, 'msg'=>'该医生不存在']);
        }
        return json(msg(1, $doctor, '医生详情'));
    }

    // 医生详情页
    public function detail()
    {
        $id = input('param.id');
        if(empty($id)){
            $this->error('404 找不到页面');
        }

        $doctor = new DoctorModel();
        $doctor = $doctor->getOneDoctor('status<>9 and id='.$id);
        if(empty($doctor)){
            $this->error('403 该医生不存在');
        }
        $this->assign('doctor', $doctor);

        return $this->fetch();
    }
}