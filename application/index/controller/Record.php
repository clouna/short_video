<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\index\controller;

use app\common\model\RecordModel;

class Record extends Hook
{
    // 上传记录
    public function upload()
    {
        $record = new RecordModel();
        $c = $record->getRecordCountToday($this->member['id']);
        if($c >= $this->member['number']){
            return json(msg(-1, '', '今日腹透次数已满！'));
        }

        $param = input('post.');
        $result = $this->validate($param, 'RecordValidate');
        if(true !== $result){
            return json(msg(-1, '', $result));
        }
        if(empty($param['cll'])){
            $param['cll'] = bcsub($param['yll'], $param['grl'], 2);
        }
        if(empty($param['hy_time'])){
            $param['hy_time'] = date('Y-m-d H:i:s');
        }

        $param['create_time'] = date('Y-m-d H:i:s');
        $flag = $record->addRecord($param);

        return json(msg($flag['code'], '', $flag['msg']));
    }
}