<?php
/**
 * Created by PhpStorm.
 * User: cui
 * Date: 2018/12/6
 * Time: 14:04
 */
namespace app\index\controller;

use app\common\model\RecordModel;

class Member extends Hook
{
    public function getLeader(){

    }

    public function info()
    {
        $record = new RecordModel();
        $this->member['today'] = $record->getRecordCountToday($this->member['id']);
        $this->member['hospital'] = '宁波鄞州第二人民医院';
        unset($this->member['id']);
        return json(msg(1, $this->member, '用户信息！'));
    }

    public function update()
    {
        $param['id'] = $this->member['id'];
        $param['name'] = input("post.name");
        $param['age'] = input("post.age");
        $param['phone'] = input("post.phone");
        $param['address'] = input("post.address");
        $param['email'] = input("post.email");
        $param['number'] = input("post.number");
        $result = $this->validate($param, 'MemberValidate');
        if(true !== $result){
            return json(msg(-1, '', $result));
        }
        $flag = $this->model->editMember($param);

        return json(msg($flag['code'], '', $flag['msg']));
    }
}