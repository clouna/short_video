<?php
/**
 * Created by PhpStorm.
 * User: win
 * Date: 2018/12/11
 * Time: 16:39
 */

namespace app\index\controller;

use app\common\model\MemberModel;
use think\Controller;

class Hook extends Controller
{
    /**
     * 用户model
     * @var MemberModel
     * @access protected
     */
    protected $model;

    /**
     * 用户信息
     * @var array
     * @access protected
     */
    protected $member;

    // 接口过滤器
    protected function _initialize()
    {
        $token = input("post.token");
        if(empty($token)){
            json(['code'=> -1,'msg'=> '未登录!'])->send();exit;
        }
        $this->model = new MemberModel();
        $this->member = $this->model->field('id,user_name card,name,age,email,phone,address,number,in_time,create_time')
            ->where(['token'=>$token, 'status'=>0])->find()->toArray();
        if(empty($this->member)){
            json(['code'=> -1,'msg'=> '账号禁止登录!'])->send();exit;
        }
    }
}