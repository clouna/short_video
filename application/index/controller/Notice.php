<?php
/**
 * Created by PhpStorm.
 * User: Clouna
 * Date: 2018/12/14
 * Time: 12:34
 */

namespace app\index\controller;

use app\common\model\NoticeModel;
use think\Controller;

class Notice extends Controller
{
    // 资讯列表
    public function noticeList()
    {
        $pageNumber = input("post.pageNumber")?:1;
        $pageCount = input("post.pageCount")?:10;

        $notice = new NoticeModel();
        $list = $notice->field('id,title,logo,create_time,status')
            ->where('status<>9')->order('status desc,zd_time desc,create_time desc')
            ->limit(($pageNumber-1)*$pageCount, $pageCount)->select();
        $total = $notice->where('status<>9')->count();

        return json([
            'code' => 1,
            'data' => $list,
            'total' => $total,
            'domain' => config('domain') . '/public/uploads/',
            'msg' => '资讯列表'
        ]);
    }

    // 资讯详情
    public function noticeDetail()
    {
        $id = input('param.id');
        if(empty($id)){
            return json(['code'=>2, 'msg'=>'参数错误']);
        }

        $notice = new NoticeModel();
        $notice = $notice->getOneNotice('status<>9 and id='.$id);
        if(empty($notice)){
            return json(['code'=>2, 'msg'=>'该资讯不存在']);
        }
        return json(msg(1, $notice, '资讯详情'));
    }

    // 资讯详情页
    public function detail()
    {
        $id = input('param.id');
        if(empty($id)){
            $this->error('404 找不到页面');
        }

        $notice = new NoticeModel();
        $notice = $notice->getOneNotice('status<>9 and id='.$id);
        if(empty($notice)){
            $this->error('403 该资讯不存在');
        }
        $this->assign('notice', $notice);

        return $this->fetch();
    }
}