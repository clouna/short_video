<?php
// +----------------------------------------------------------------------
// | guoliduo
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
return [
    // 模板参数替换
    'view_replace_str'       => array(
        '__BOOTSTRAP__'    => '/public/static/admin/bootstrap',
        '__STATIC__' => '/public/static',
        '__CSS__'    => '/public/static/admin/css',
        '__JS__'     => '/public/static/admin/js',
        '__IMG__' => '/public/static/admin/images',
        '__UPLOAD__' => '/public/uploads/',
        '__SUMMERNOTE__' => '/public/summerNote/',
        '__DATEPICKER__' => '/public/static/datePicker'
    ),

    // 管理员状态
    'user_status' => [
        '0' => '正常',
        '1' => '禁用'
    ],

    // 角色状态
    'role_status' => [
        '0' => 'yes.png',//启动
        '1' => 'cancel.png'//禁用
    ],

    // 配送方式
    'send_type' => [
        '0' => '快递或自提',
        '1' => '仅快递',
        '2' => '仅自提'
    ],

    // 团长抽成设置
    'leader' => [
        '0' => '不单独设置',
        '1' => '比例',
        '2' => '固定金额'
    ],

    //备份数据地址
    'back_path' => APP_PATH .'../back/'
];
