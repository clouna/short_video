<?php
// +----------------------------------------------------------------------
// | guoliduo
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class RoleModel extends Model
{
    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_role';
        parent::__construct($data);
    }

    /**
     * 获取角色信息
     * @param $id
     * @return array
     */
    public function getRoleInfo($id)
    {
        $result = $this->where('id', $id)->find()->toArray();
        // 超级管理员权限是 *
        if(empty($result['rule'])){
            $result['action'] = '';
            return $result;
        }else if('*' == $result['rule']){
            $where = '';
        }else{
            $where = 'id in(' . $result['rule'] . ')';
        }

        // 查询权限节点
        $nodeModel = new NodeModel();
        $res = $nodeModel->getActions($where);

        foreach($res as $key=>$vo){
            $result['action'][] = strtolower($vo['mca']);
        }

        return $result;
    }

    /**
     * 获取所有角色绑定的会员数
     * @param $where
     * @return int|string
     */
    public function getRoleMember()
    {
        return $this->field('r.*,count(u.role_id) num')->alias('r')
            ->join('__USER__ u','u.role_id=r.id','LEFT')
            ->group('r.id')->order('r.id')->select();
    }

    /**
     * 插入角色信息
     * @param $param
     * @return array
     */
    public function insertRole($param)
    {
        try{
            $result =  $this->validate('RoleValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{
                return msg(1, url('role/index'), '添加角色成功');
            }
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑角色信息
     * @param $param
     * @return array
     */
    public function editRole($param)
    {
        try{
            $result = $this->validate('RoleValidate')->save($param, ['id' => $param['id']]);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('role/index'), '编辑角色成功');
            }
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 删除角色
     * @param $id
     * @return array
     */
    public function delRole($id)
    {
        try{
            $this->where('id', $id)->delete();
            return msg(1, '', '删除角色成功');
        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据角色id获取角色信息
     * @param $id
     * @return array
     */
    public function getOneRole($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 获取所有的角色信息
     * @return array
     */
    public function getRole()
    {
        return $this->select();
    }

    /**
     * 获取角色的权限节点
     * @param $id
     * @return mixed
     */
    public function getRuleById($id)
    {
        $res = $this->field('rule')->where('id', $id)->find();
        return $res['rule'];
    }

    /**
     * 分配权限
     * @param $param
     * @return array
     */
    public function editAccess($param)
    {
        try{
            $this->save($param, ['id' => $param['id']]);
            return msg(1, '', '分配权限成功');
        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }
}