<?php
// +----------------------------------------------------------------------
// | guoliduo
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class VideoModel extends Model
{
    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_video';
        parent::__construct($data);
    }

    /**
     * 返回原有数据  不自动进行时间转换
     * @param $time
     * @return mixed
     */
    public function getCreateTimeAttr($time)
    {
        return $time;
    }

    /**
     * 短视频列表
     * @param $where array 筛选
     * @param $order string 排序
     * @param $pageNum int 每页行数
     * @return array
     */
    public function getVideoPage($where=[], $order='id', $pageNum = 7)
    {
        return $this->where($where)->order($order)->paginate($pageNum);
    }

    /**
     *
     * 根据搜索条件获取用户列表信息
     * @param $where
     * @return array
     */
    public function getUsersByWhere($where)
    {
        return $this->alias('u')
            ->field('u.id,u.user_name,u.last_login_ip,from_unixtime(u.last_login_time) time,'
                .'u.real_name,u.status,u.role_id,role_name')
            ->join('__ROLE__ r', 'u.role_id = r.id', 'LEFT')
            ->where($where)->order('role_id')->paginate();
    }

    /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     * @return int|string
     */
    public function getAllUsers($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 插入管理员信息
     * @param $param
     * @return array
     */
    public function insertUser($param)
    {
        try{
            $result = $this->validate('UserValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('user/index'), '添加用户成功');
            }
        }catch(\PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑管理员信息
     * @param $param
     * @return array
     */
    public function editUser($param)
    {
        try{

            $result =  $this->validate('UserValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('user/index'), '编辑用户成功');
            }
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据管理员id获取角色信息
     * @param array $where
     * @return array
     */
    public function getOneVideo($where)
    {
        return $this->where($where)->find();
    }

    /**
     * 删除管理员
     * @param $id
     * @return array
     */
    public function delUser($id)
    {
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除管理员成功');

        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }


    /**
     * 根据用户名获取管理员信息
     * @param $name
     * @return Model
     */
    public function findUserByName($name)
    {
        return $this->where('user_name', $name)->find();
    }

    /**
     * 更新管理员状态̬
     * @param array $param
     * @param $uid
     * @return array
     */
    public function updateStatus($param = [], $uid)
    {
        try{
            $this->where('id', $uid)->update($param);
            return msg(1, '', 'ok');
        }catch (\Exception $e){

            return msg(-1, '', $e->getMessage());
        }
    }
}