<?php
// +----------------------------------------------------------------------
// | guoliduo
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class NodeModel extends Model
{
    /**
     * 构造方法
     * @access public
     * @param array|object $data 数据
     */
    public function __construct($data = []){
        $this->table = PREFIX.'_node';
        parent::__construct($data);
    }

    /**
     * 根据节点数据获取对应的菜单
     * @param string $nodeStr
     * @return array
     */
    public function getMenu($nodeStr = '')
    {
        if(empty($nodeStr)){
            return [];
        }
        // 超级管理员没有节点数组 * 号表示
        $where = '*' == $nodeStr ? 'is_menu = 2' : 'is_menu = 2 and id in(' . $nodeStr . ')';

        $result = $this->field('id,node_name,type_id,mca,module')
            ->where($where)->order('number')->select();
        $menu = prepareMenu($result);

        return $menu;
    }

    /**
     * 根据条件获取访问权限节点数据
     * @param $where
     * @return array
     */
    public function getActions($where)
    {
        return $this->field('mca')->where($where)->select();
    }

    /**
     * 根据条件获取访问权限节点名称
     * @param $mca
     * @return array
     */
    public function getActionName($mca)
    {
        return $this->field('node_name')->where(['mca'=>$mca])->find();
    }

    /**
     * 获取节点数据
     * @return mixed
     */
    public function getNodeList()
    {
        return $this->field('id,node_name name,type_id pid,number,mca')->order('number')->select();
    }

    /**
     * 插入节点信息
     * @param $param
     * @return array
     */
    public function insertNode($param)
    {
        try{
            $this->save($param);
            return msg(1, '', '添加节点成功');
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑节点
     * @param $param
     * @return array
     */
    public function editNode($param)
    {
        try{
            $this->save($param, ['id' => $param['id']]);
            return msg(1, '', '编辑节点成功');
        }catch(\PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 删除节点
     * @param $id
     * @return array
     */
    public function delNode($id)
    {
        try{
            $this->where('id', $id)->delete();
            return msg(1, '', '删除节点成功');
        }catch(\PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 获取节点数据
     * @param $id
     * @return string
     */
    public function getNodeInfo($rule)
    {
        $result = $this->field('id,node_name,type_id pid')->select();
        $str = '';

        if(!empty($rule)){
            $rule = explode(',', $rule);
        }

        foreach($result as $key=>$vo){
            if(!empty($rule) && in_array($vo['id'], $rule)){
                $result[$key]['checked'] = 1;
            }else{
                $result[$key]['checked'] = 0;
            }
            $str .= '},';
        }

        return getTree(objToArray($result), false);
    }
}