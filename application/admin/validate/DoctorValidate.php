<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class DoctorValidate extends Validate
{
    protected $rule = [
        ['name', 'require', '姓名不能为空'],
        ['logo', 'file|fileExt:jpg,png,gif|fileSize:10', '头像不能为空|头像格式不对|头像超过1M'],
        ['skill', 'require', '擅长不能为空'],
        ['content', 'require', '详情不能为空']
    ];

}