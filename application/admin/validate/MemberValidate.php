<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class MemberValidate extends Validate
{
    protected $rule = [
        ['user_name', 'require|number|length:18|unique:member',
            '身份证号不能为空|身份证号格式不对|身份证号格式不对|身份证号已注册，请换个身份证号'],
        ['password', 'require', '密码不能为空'],
        ['register_time', 'require|dateFormat:Y-m-d H:i:s', '注册日期不能为空|注册日期格式不对']
    ];
}