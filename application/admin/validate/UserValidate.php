<?php
// +----------------------------------------------------------------------
// | snake
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $rule = [
        ['user_name', 'require|alphaDash|unique:user', '名称不能为空|名称必须为字母和数字，下划线及破折号的组合|管理员已经存在'],
        ['real_name', 'require|chs', '真实姓名不能为空|真实名称只支持中文'],
        ['phone', 'require|number', '联系方式不能为空|联系方式格式不对'],
        ['email', 'email', '邮箱格式不对']
    ];

}