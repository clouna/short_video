<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class NoticeValidate extends Validate
{
    protected $rule = [
        ['title', 'require', '标题不能为空'],
        ['logo', 'file|fileExt:jpg,png,gif|fileSize:10', '图片不能为空|图片格式不对|图片超过1M'],
        ['source', 'require', '资讯来源不能为空'],
        ['content', 'require', '资讯内容不能为空']
    ];

}