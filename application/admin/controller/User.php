<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\RoleModel;
use app\admin\model\UserModel;

class User extends Base
{
    //用户列表
    public function index()
    {
        $param = input('param.');
        $where = [];
        if (!empty($param['searchText'])) {
            $where['user_name'] = ['like', '%' . $param['searchText'] . '%'];
        }
        $user = new UserModel();
        $selectResult = $user->getUsersByWhere($where);
        // 拼装参数
        foreach($selectResult as $key=>$vo){
            if($vo['id'] == 1){
                $selectResult[$key]['operate'] = '<a href="javascript:reset(2)"><button class="btn btn-danger btn-sm">重置密码</button></a>';
            }else{
                $selectResult[$key]['operate'] = $this->makeButton($vo['id']);
            }
        }
        $this->assign('list', $selectResult);

        return $this->fetch();
    }

    // 重置密码
    public function reset()
    {
        $old_p = input('post.old_p');
        $new_p = input('post.new_p');
        $new_q = input('post.new_q');

        $user = new UserModel();
        $info = $user->getOneUser(1);
        if($info['password'] != md5($old_p)){
            return json(msg(-2, '', '原密码不对'));
        }elseif($new_p != $new_q){
            return json(msg(-2, '', '两次新密码不一致'));
        }elseif($old_p == $new_p){
            return json(msg(-2, '', '请换一个新密码'));
        }else{
            $param['password'] = md5($new_p);
            $param['id'] = $info['id'];
            $flag = $user->save(['password'=>md5($new_p)], ['id' => $info['id']]);
            if(empty($flag)){
                return json(msg(-1, '', '重置失败'));
            }else{
                return json(msg(1, '', '重置成功'));
            }
        }
    }

    // 添加用户
    public function userAdd()
    {
        if(request()->isPost()){

            $param = input('post.');

            $param['password'] = md5($param['password']);
            $user = new UserModel();
            $flag = $user->insertUser($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $role = new RoleModel();
        $this->assign([
            'role' => $role->getRole(),
            'status' => config('user_status')
        ]);

        return $this->fetch();
    }

    // 编辑用户
    public function userEdit()
    {
        $user = new UserModel();

        if(request()->isPost()){

            $param = input('post.');

            if(empty($param['password'])){
                unset($param['password']);
            }else{
                $param['password'] = md5($param['password']);
            }
            $flag = $user->editUser($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $id = input('param.id');
        $role = new RoleModel();

        $this->assign([
            'user' => $user->getOneUser($id),
            'status' => config('user_status'),
            'role' => $role->getRole()
        ]);
        return $this->fetch();
    }

    // 删除用户
    public function userDel()
    {
        $id = input('param.id');

        $role = new UserModel();
        $flag = $role->delUser($id);
        return json(msg($flag['code'], $flag['data'], $flag['msg']));
    }

    /**
     * 拼装操作按钮
     * @param $id
     * @return array
     */
    private function makeButton($id)
    {
        return showOperate([
            '编辑' => [
                'auth' => 'user/useredit',
                'href' => url('user/userEdit', ['id' => $id]),
                'btnStyle' => 'primary',
                'icon' => 'fa fa-paste'
            ],
            '删除' => [
                'auth' => 'user/userdel',
                'href' => "javascript:userDel(" .$id .")",
                'btnStyle' => 'danger',
                'icon' => 'fa fa-trash-o'
            ]
        ]);
    }
}
