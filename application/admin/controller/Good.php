<?php
/**
 * Created by PhpStorm.
 * User: cuishenghu
 * Date: 2018/12/6
 * Time: 14:35
 */

namespace app\admin\controller;

use app\common\model\ConfigModel;
use app\common\model\MemberModel;
use app\common\model\GoodModel;
use think\Controller;

class Good extends Base
{
    //商品列表
    public function index()
    {
        $product = new GoodModel();
        $selectResult = $product->getPageList([], 'status desc,number desc');
        // 拼装参数
        foreach($selectResult as $key=>$vo){
            $selectResult[$key]['operate'] = $this->makeButton($vo);
        }
        $this->assign('list', $selectResult);

        return $this->fetch();
    }

    //商品设置
    public function edit()
    {
        $id = input('param.id');
        $category = new GoodModel('category');
        if(!empty($id)){
            $current = $category->getOne(['id'=>$id]);
            $this->assign([
                'category' => $current
            ]);
        }
        if(request()->isAjax()){
            $param = input('post.');
            if(strlen($param['user_name']) < 18){
                return json(['code'=>-2, 'msg'=>'身份证号格式不对！']);
            }
            $member = new MemberModel();
            $info = $member->getMemberInfo([
                'user_name'=>$param['user_name'],
                'id'=>['<>', $param['id']]
            ], 'user_name');
            if(!empty($info)){
                return json(['code'=>-3, 'msg'=>'身份证号已注册，请换个身份证号！']);
            }
            $flag = $member->editMember($param);

            return json(msg($flag['code'], '', $flag['msg']));
        }
        return $this->fetch();
    }

    // 活动设置
    public function activity(){
        $config = new ConfigModel();
        $configs = [
            'index_hot_switch'=>0,
            'index_today_switch'=>0,
        ];
        $list = [];
        if(request()->isAjax()){
            $param = input('post.');
            $list = [];
            foreach ($configs as $key => $value) {
                $list[] = $config->full_id($key, $param[$key], $value);
            }
            $ret = $config->allowField(true)->saveAll($list);

            if($ret){
                return json(msg(1, '', '保存成功！'));
            }else{
                return json(msg(-1, '', '保存失败'));
            }
        }
        foreach ($configs as $key => $value) {
            $list[$key] = $config->get_value($key, $value);
        }
        $this->assign($list);
        return $this->fetch();
    }

    // 删除患者
    public function delete()
    {
        $id = input('param.id');

        $role = new MemberModel();
        $result = $role->where('id', $id)->delete();
        if($result){
            return json(msg(1, '', '删除患者成功'));
        }else{
            return json(msg(1, '', '删除患者失败'));
        }
    }

    /**
     * 拼装操作按钮
     * @param $item
     * @return array
     */
    private function makeButton($item)
    {
        return showOperate([
            '重置密码' => [
                'auth' => 'member/edit',
                'href' => "javascript:reset(" .$item['id'] .")",
                'btnStyle' => 'success'
            ],
            '修改' => [
                'auth' => 'member/edit',
                'href' => "javascript:edit(" . $item['id'] .",'".$item['user_name']."','".$item['register_time']."')",
                'btnStyle' => 'primary'
            ],
            '删除' => [
                'auth' => 'member/delete',
                'href' => "javascript:deleteMember(" .$item['id'] .")",
                'btnStyle' => 'danger'
            ]
        ]);
    }
}