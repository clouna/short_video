<?php
// +----------------------------------------------------------------------
// | guoliduo
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\NodeModel;
use app\admin\model\UserModel;
use app\admin\model\RoleModel;
use think\Controller;

/**
 * Class Index
 * @package app\admin\controller
 */
class Index extends Base
{
    public function index()
    {
        // 刷新用户信息
        $user = new UserModel();
        $info = $user->getOneUser(session('id'));

        // 刷新权限菜单
        $role = new RoleModel();
        $actions = $role->getRoleInfo($info['role_id']);
        session('role', $actions['role_name']);  // 角色名
        session('rule', $actions['rule']);  // 角色节点
        session('action', $actions['action']);  // 角色权限

        $node = new NodeModel();
        $menu = $node->getMenu($info['rule']);
        $this->assign([
            'menu' => $menu,
            'module' => ''//菜单分模块
        ]);

        return $this->fetch();
    }

    /**
     * 后台默认首页
     * @return mixed
     */
    public function welcome()
    {
        $this->assign('sys_info',$this->get_sys_info());
        $week = array('星期一','星期二','星期三','星期四','星期五','星期六','星期日');
        $date['week'] = $week[date("w")];
        $date['year'] = date("Y");
        $date['day'] = date("年m月d日");
        $date['time'] = date("G:i");
        $this->assign('date',$date);
        $this->display();
        return $this->fetch();
    }

    /**
     * 系统信息
     * @return [type] [description]
     */
    public function get_sys_info(){
        $sys_info['os']             = PHP_OS;
        $sys_info['zlib']           = function_exists('gzclose') ? 'YES' : 'NO';//zlib
        $sys_info['safe_mode']      = (boolean) ini_get('safe_mode') ? 'YES' : 'NO';//safe_mode = Off
        $sys_info['timezone']       = function_exists("date_default_timezone_get") ? date_default_timezone_get() : "no_timezone";
        $sys_info['curl']           = function_exists('curl_init') ? 'YES' : 'NO';
        $sys_info['web_server']     = $_SERVER['SERVER_SOFTWARE'];
        $sys_info['phpv']           = phpversion();
        $sys_info['ip']             = GetHostByName($_SERVER['SERVER_NAME']);
        $sys_info['fileupload']     = @ini_get('file_uploads') ? ini_get('upload_max_filesize') :'unknown';
        $sys_info['max_ex_time']    = @ini_get("max_execution_time").'s'; //脚本最大执行时间
        $sys_info['set_time_limit'] = function_exists("set_time_limit") ? true : false;
        $sys_info['domain']         = $_SERVER['HTTP_HOST'];
        $sys_info['memory_limit']   = ini_get('memory_limit');
        $sys_info['mysql_version']  = '';
        if(function_exists("gd_info")){
            $gd = gd_info();
            $sys_info['gdinfo']     = $gd['GD Version'];
        }else {
            $sys_info['gdinfo']     = "未知";
        }
        return $sys_info;
    }

    /**
     * ajax 修改指定表数据字段  一般修改状态 比如 是否推荐 是否开启 等 图标切换的
     * table,id_name,id_value,field,value
     */
    public function changeTableVal(){
        $table = input('table'); // 表名
        $id_name = input('id_name'); // 表主键id名
        $id_value = input('id_value'); // 表主键id值
        $field  = input('field'); // 修改哪个字段
        $value  = input('value'); // 修改字段值
        model($table)->where("$id_name = $id_value")->update([$field=>$value]); // 根据条件保存修改的数据
        return json(msg(1, '', 'ok'));
    }
}