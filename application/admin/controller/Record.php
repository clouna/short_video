<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\model\MemberModel;
use app\common\model\RecordModel;

class Record extends Base
{
    //患者列表
    public function index()
    {
        $whe = ['status'=>0];
        $name = input('post.name');
        $card = input('post.card');
        if(!empty($name)){
            $whe['name'] = ['LIKE', $name];
        }
        if(!empty($card)){
            $whe['user_name'] = ['LIKE', $card];
        }

        $member = new MemberModel();
        $selectResult = $member->getMemberPage($whe, 'register_time desc');
        $this->assign('name', $name);
        $this->assign('card', $card);
        $this->assign('list', $selectResult);

        return $this->fetch();
    }

    // 患者详情
    public function record()
    {
        $id = input('param.id')?:0;
        $whe = ['mid'=>$id];
        $start = input('post.start');
        $end = input('post.end');
        if(!empty($start)){
            $whe['hy_time'] = ['EGT', $start];
        }
        if(!empty($end)){
            $whe['hy_time'] = ['ELT', $end];
        }

        $record = new RecordModel();
        $selectResult = $record->getRecordPage($whe, 'create_time desc');
        $this->assign('start', $start);
        $this->assign('end', $end);
        $this->assign('list', $selectResult);

        return $this->fetch();
    }
}