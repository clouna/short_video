<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.csh.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\model\SystemModel;

class System extends Base
{
    //基础设置
    public function index()
    {
        $system = new SystemModel();
        if(request()->isPost()){
            $param = input('post.');
            $flag = $system->editSystem($param);

            return json(msg($flag['code'], 0, $flag['msg']));
        }
        $this->assign([
            'info' => $system->getSystem()
        ]);
        return $this->fetch();
    }

    // 小程序授权
    public function smallApp()
    {
        $system = new SystemModel();
        if(request()->isPost()){
            $param = input('post.');
            $flag = $system->editSystem($param);

            return json(msg($flag['code'], 0, $flag['msg']));
        }
        $this->assign([
            'info' => $system->getSystem()
        ]);
        return $this->fetch();
    }

    // 模板消息
    public function template()
    {
        $system = new SystemModel();
        if(request()->isPost()){
            $param = input('post.');
            $save['template'] = [
                'pay' => $param['pay'],
                'arrival' => $param['arrival'],
                'return' => $param['return']
            ];
            $flag = $system->editSystem($save);

            return json(msg($flag['code'], 0, $flag['msg']));
        }
        $system = $system->getSystem();
        $this->assign([
            'info' => json_decode($system['template'], true)
        ]);
        return $this->fetch();
    }
}