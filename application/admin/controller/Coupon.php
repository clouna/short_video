<?php
/**
 * Created by PhpStorm.
 * User: win
 * Date: 2019/8/19
 * Time: 11:05
 */

namespace app\admin\controller;

use app\common\model\BaseModel;

class Coupon extends Base
{
    //优惠券列表
    public function index()
    {
        $coupon = new BaseModel('coupon');
        $selectResult = $coupon->getPageList([], 'state desc');
        // 拼装参数
        foreach($selectResult as $key=>$vo){
            $selectResult[$key]['operate'] = $this->makeButton($vo);
        }
        $this->assign('list', $selectResult);

        return $this->fetch();
    }
}