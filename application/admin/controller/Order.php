<?php
/**
 * Created by PhpStorm.
 * User: win
 * Date: 2019/8/19
 * Time: 9:32
 */

namespace app\admin\controller;

use app\common\model\OrderModel;

class Order extends Base
{
    //订单列表
    public function index()
    {
        $orders = new OrderModel();
        $selectResult = $orders->getPageList([], 'update_time desc');
        // 拼装参数
        foreach($selectResult as $key=>$vo){
            $selectResult[$key]['operate'] = $this->makeButton($vo);
        }
        $this->assign('list', $selectResult);

        return $this->fetch();
    }
}