<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\NodeModel;
use think\Controller;

class Base extends Controller
{
    public function _initialize()
    {
        // 判断用户是否登录
        if(empty(session('id'))){
            $loginUrl = url('login/index');
            if(request()->isAjax()){
                return msg(201, $loginUrl, '登录超时');
            }

            $this->redirect($loginUrl);
        }

        // 检测权限
        $control = lcfirst(request()->controller());
        $action = lcfirst(request()->action());

        if(empty(authCheck($control . '/' . $action))){
            if(request()->isAjax()){
                return msg(403, '', '您没有权限');
            }

            $this->error('403 您没有权限');
        }

        $node = new NodeModel();
        $title = $node->getActionName($control . '/' . $action);

        $this->assign([
            'username' => session('username'),
            'rolename' => session('rolename'),
            'mca' => $control . '/' . $action,
            'title' => $title['node_name']
        ]);
    }

    /**
     * 拼装操作按钮
     * @param $item
     * @return array
     */
    private function verifyButtom($item)
    {
        $operate = ['add', 'edit', 'remove', 'detail'];
        $request = request();
        $module     = $request->module();
        $controller = $request->controller();
        foreach($operate as $item){
            check_action_exists($request, $module, $controller, $item);
        }
    }
}