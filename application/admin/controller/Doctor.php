<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\model\DoctorModel;
use think\File;

class Doctor extends Base
{
    //医生列表
    public function index()
    {
        $doctor = new DoctorModel();
        $selectResult = $doctor->getDoctorPage(['status'=>['<>',9]], 'create_time desc');
        foreach($selectResult as $key=>$vo){
            $selectResult[$key]['operate'] = $this->makeButton($vo['id']);
        }
        $this->assign('list', $selectResult);

        return $this->fetch();
    }

    // 维护信息
    public function edit()
    {
        $id = input('param.id');
        $doctor = new DoctorModel();
        if(!empty($id)){
            $current = $doctor->getOneDoctor(['id'=>$id]);
            $this->assign([
                'doctor' => $current
            ]);
        }
        if(request()->isPost()){
            $param = input('post.');
            $result = $this->validate($param, 'DoctorValidate');
            if(true !== $result){
                return json(msg(-1, '', $result));
            }
            // 获取logo文件
            $img = request()->file('logo');
            if($img){
                $info = $img->move(ROOT_PATH . config('uploads'), empty($current['logo'])?date('Ym').'/'.uniqid():$current['logo']);
                if($info){
                    $param['logo'] = $info->getSaveName();
                }
            }else{
                unset($param['logo']);
            }
            if(empty($id)){
                $param['create_time'] = date('Y-m-d H:i:s');
            }
            $flag = $doctor->editDoctor($param);

            return json(msg($flag['code'], $param['id']?:0, $flag['msg']));
        }
        return $this->fetch();
    }

    public function updateFile(){
        // 获取富文本图片
        $files = request()->file('img');
        $src = [];
        foreach($files as $img){
            if ($img instanceof File) {
                $info = $img->move(ROOT_PATH . config('summerNote'), date('Ym').'/'.uniqid());
                if($info){
                    $src[] = $info->getSaveName();
                    //$src[] = config('domain').$info->getSaveName();
                }
            }
        }
        return json(msg(1, $src, ''));
    }

    // 删除
    public function delete(){
        $doctor = new DoctorModel();
        $flag = $doctor->save(['status' => 9], ['id'=>input('param.id')]);
        return json(msg(empty($flag)?2:1, '',empty($flag)?'删除失败':'删除成功'));
    }

    /**
     * 拼装操作按钮
     * @param $id
     * @return array
     */
    private function makeButton($id)
    {
        return showOperate([
            '修改' => [
                'auth' => 'doctor/edit',
                'href' => url('doctor/edit', ['id' => $id]),
                'btnStyle' => 'primary'
            ],
            '删除' => [
                'auth' => 'doctor/delete',
                'href' => "javascript:deleteDoctor(" .$id .")",
                'btnStyle' => 'danger'
            ]
        ]);
    }
}