<?php
// +----------------------------------------------------------------------
// | guoliduo
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://www.guoliduo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Csh <1317841280@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\NodeModel;
use app\admin\model\RoleModel;

class Role extends Base
{
    // 角色列表
    public function index()
    {
        $role = new RoleModel();
        $roles = $role->getRoleMember();
        $roles = objToArray($roles);
        // 拼装参数
        foreach($roles as $key=>$vo){
            $roles[$key]['operate'] = $this->makeButton($vo);
        }

        $this->assign([
            'roles' => $roles
        ]);
        return $this->fetch();
    }

    // 添加角色
    public function roleAdd()
    {
        if(request()->isPost()){
            $param = input('post.');

            $role = new RoleModel();
            $flag = $role->insertRole($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }
        return $this->fetch();
    }

    // 编辑角色
    public function roleEdit()
    {
        $role = new RoleModel();

        if(request()->isPost()){

            $param = input('post.');
            $flag = $role->editRole($param);

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $id = input('param.id');
        $this->assign([
            'role' => $role->getOneRole($id)
        ]);
        return $this->fetch();
    }

    // 删除角色
    public function roleDel()
    {
        $id = input('param.id');

        $role = new RoleModel();
        $flag = $role->delRole($id);
        return json(msg($flag['code'], $flag['data'], $flag['msg']));
    }

    // 分配权限
    public function access()
    {
        $param = input('param.');
        $role = new RoleModel();

        // 分配新权限
        if(request()->isPost()){
            $data = [
                'id' => $param['id'],
                'rule' => implode(',', $param['rule_ids'])
            ];
            $flag = $role->editAccess($data);
            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }

        $node = new NodeModel();
        // 获取现在的权限
        $rule = $role->getOneRole($param['id']);
        $this->assign([
            'rule' => $rule,
            'nodes' => $node->getNodeInfo($rule['rule'])
        ]);
        return $this->fetch();
    }

    /**
     * 拼装操作按钮
     * @param $item
     * @return string
     */
    private function makeButton($item)
    {
        return showOperate([
            '编辑' => [
                'auth' => 'role/roleedit',
                'href' => "javascript:edit(" . $item['id'] .",'".$item['role_name']."',".$item['status'].")",
                'btnStyle' => 'primary'
            ],
            '删除' => [
                'auth' => 'role/roledel',
                'href' => "javascript:delRole(" . $item['id'] .")",
                'btnStyle' => 'danger'
            ],
            '分配权限' => [
                'auth' => 'role/access',
                'href' => url('role/access', ['id' => $item['id']]),
                'btnStyle' => 'inverse'
            ]
        ]);
    }
}