<?php
/**
 * Created by PhpStorm.
 * User: cuishenghu
 * Date: 2018/12/6
 * Time: 14:35
 */

namespace app\admin\controller;

use app\common\model\MemberModel;
use think\Controller;

class Member extends Base
{
    //患者列表
    public function index()
    {
        $member = new MemberModel();
        $selectResult = $member->getMemberPage(['status'=>0], 'register_time desc');
        // 拼装参数
        foreach($selectResult as $key=>$vo){
            $selectResult[$key]['operate'] = $this->makeButton($vo);
        }
        $this->assign('list', $selectResult);

        return $this->fetch();
    }

    //患者登记
    public function addMember()
    {
        if(request()->isAjax()){
            $param = input('post.');
            if(strlen($param['user_name']) >= 18){
                $param['password'] = md5(substr($param['user_name'], -6));//取身份证号后六位
            }
            $param['create_time'] = date('Y-m-d H:i:s');
            $member = new MemberModel();
            $flag = $member->insertMember($param);
            if(is_array($flag['data'])){
                $flag['data'] = '患者编码：'.$flag['data']['id'].'<br>'.
                    '患者身份证号：'.$flag['data']['user_name'].'<br>'.
                    '患者密码：'.substr($flag['data']['user_name'], -6);
            }

            return json(msg($flag['code'], $flag['data'], $flag['msg']));
        }else{
            return $this->fetch();
        }
    }

    //患者修改
    public function edit()
    {
        if(request()->isAjax()){
            $param = input('post.');
            if(strlen($param['user_name']) < 18){
                return json(['code'=>-2, 'msg'=>'身份证号格式不对！']);
            }
            $member = new MemberModel();
            $info = $member->getMemberInfo([
                'user_name'=>$param['user_name'],
                'id'=>['<>', $param['id']]
            ], 'user_name');
            if(!empty($info)){
                return json(['code'=>-3, 'msg'=>'身份证号已注册，请换个身份证号！']);
            }
            $flag = $member->editMember($param);

            return json(msg($flag['code'], '', $flag['msg']));
        }else{
            return json(['code'=>-1, 'msg'=>'请求无效！']);
        }
    }

    // 重置密码
    public function reset(){
        $member = MemberModel::get(input('param.id'));
        $member->password = md5(substr($member->user_name, -6));
        if($member->save()){
            return json(msg(1, '', '重置成功'));
        }else{
            return json(msg(-1, '', '重置失败'));
        }
    }

    // 删除患者
    public function delete()
    {
        $id = input('param.id');

        $role = new MemberModel();
        $result = $role->where('id', $id)->delete();
        if($result){
            return json(msg(1, '', '删除患者成功'));
        }else{
            return json(msg(1, '', '删除患者失败'));
        }
    }

    /**
     * 拼装操作按钮
     * @param $id
     * @return array
     */
    private function makeButton($item)
    {
        return showOperate([
            '重置密码' => [
                'auth' => 'member/edit',
                'href' => "javascript:reset(" .$item['id'] .")",
                'btnStyle' => 'success'
            ],
            '修改' => [
                'auth' => 'member/edit',
                'href' => "javascript:edit(" . $item['id'] .",'".$item['user_name']."','".$item['register_time']."')",
                'btnStyle' => 'primary'
            ],
            '删除' => [
                'auth' => 'member/delete',
                'href' => "javascript:deleteMember(" .$item['id'] .")",
                'btnStyle' => 'danger'
            ]
        ]);
    }
}