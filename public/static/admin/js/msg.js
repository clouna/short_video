/**
 * Created by csh on 2018/12/8.
 */
function showSuccess(res){
    if(1 == res.code){
        window.location.reload();
    }else{
        layer.alert(res.msg, {icon:2});
    }
}