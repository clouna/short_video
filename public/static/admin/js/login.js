$(document).ready(function() {
	$(".btn-submit").click(function(){
		login();
	});
	$('#form_login').keydown(function(event){
        if (event.keyCode == 13) {
            $('.btn-submit').click();
        }
    });
});
function login(){
	var username = $('#admin_name');
	var password = $('#admin_password');
	var captcha = $('#captcha');
	if(username.val() == ''){
        layer.msg('账号不能为空');
		username.focus();
        return false;
    }
    if(password.val() == ''){
        layer.msg('密码不能为空');
		password.focus();
        return false;
    }

    if(captcha.val() == ''){
        layer.msg('验证码不能为空');
		captcha.focus();
        return false;
    }

    $.post(loginHandleUrl,{'admin_name':username.val(),'admin_password':password.val(),'captcha':captcha.val()},function(data){
		if(!data.code){
			setTimeout(function(){
				$('.input-username,.dot-left').addClass('animated fadeOutRight')
			    $('.input-password-box,.dot-right').addClass('animated fadeOutLeft')
			    $('.btn-submit').addClass('animated fadeOutUp')
			    setTimeout(function () {
			        $('.avatar').addClass('avatar-top');
			        $('.submit').hide();
			        $('.submit2').html('<div class="progress"><div class="progress-bar progress-bar-success" aria-valuetransitiongoal="100"></div></div>');
			        layer.msg(data.msg);
			        $('.progress .progress-bar').progressbar({done : function() {
			            location.href = '/admin';
			        }});
			    },300);
			},300)
		}else{
			layer.msg(data.msg);
            $('#codeimage').attr('src', verifyUrl);
		}
	},'json')
}