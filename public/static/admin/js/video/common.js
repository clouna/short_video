var apiUrl = $("body").data('api');
var apiServiceUrl = $("body").data('service-api');
var cdnUrl = $("body").data('cdn-url');
var roleName = $("body").data('role-name');
var _wid = $("body").data('wid');

Vue.prototype.module = $("body").data('module');

//Vue.prototype.supplyStreamerId = parseInt(supplyStreamerId);
//Vue.prototype.cloudId = parseInt(cloudId);
//Vue.prototype.supplyStatus = parseInt(supplyStatus);
//
////TODO 是否是30w总部商户 1
//let _isSubMain = $("body").data('is-submain') || 0;
//Vue.prototype.isSubMain = parseInt(_isSubMain);
//
//let _isAdmin = $("body").data('is-admin') == '1' ? true : false;
//Vue.prototype.isAdmin = _isAdmin
//
//
//let _is30w = $("body").data('is-is30w') == '1' ? true : false;
//Vue.prototype.is30w = _is30w
//
//let _isWp = $("body").data('iswp') == '1' ? true : false;
//Vue.prototype.isWp = _isWp
//
//Vue.prototype.appId = appId
//
//const STR_PAD_LEFT = 1; //向左扩展字符串
//const STR_PAD_RIGHT = 2; //向右扩展字符串
//
//let pad = (string, length, position) => {
//    let len = String(string).length,
//        mark = '0'
//
//    length = length ? length : 0
//
//    if (len >= length) {
//        return string
//    }
//
//    position = position && parseInt(position) === STR_PAD_RIGHT ? STR_PAD_RIGHT : STR_PAD_LEFT
//
//    return position === STR_PAD_LEFT ? (Array(length).join(mark) + string).slice(-length) : (string + Array(length - len + 1).join(mark))
//}
//
//let date = (format, timestamp) => {
//    let _date = timestamp ? new Date(timestamp * 1000) : new Date(),
//        o = {
//            Y: () => _date.getFullYear(), // Y: 2001 or 1999  y: 01 or 99
//            m: () => pad(_date.getMonth() + 1, 2),// month m: 01 to 12  n: 1 to 12
//            n: () => _date.getMonth() + 1,
//            j: () => _date.getDate(),// d: 01 to 31 j: 1 to 31
//            d: () => pad(_date.getDate(), 2),
//            G: () => _date.getHours(),// H: 00~23  G: 0 ~ 23
//            H: () => pad(_date.getHours(), 2),
//            i: () => pad(_date.getMinutes(), 2), //i: 00~59
//            s: () => pad(_date.getSeconds(), 2), //s: 00 ~ 59
//            D: () => {
//                let week = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
//                    day = _date.getDay();
//                return week[day]
//            },
//            w: () => {
//                let week = ['日', '一', '二', '三', '四', '五', '六'],
//                    day = _date.getDay();
//                return week[day];
//            }
//        }
//
//    for (let key in o) {
//        format = format.replace(new RegExp(key), o[key])
//    }
//    return format
//}

var axiosService = axios.create({
    baseURL: apiServiceUrl,
    timeout: 10000,
    headers: {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
});
// 添加请求拦截器
axiosService.interceptors.request.use(function (config) {
    config.data = Qs.stringify(config.data);
    return config;
}, function (error) {
    return Promise.reject(error);
});

var axios = axios.create({
    baseURL: apiUrl,
    timeout: 10000,
    headers: {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
});

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
   var baseData = {wid: _wid};
   var data = Object.assign(baseData, config.data)
   config.data = Qs.stringify(data);
   return config;
}, function (error) {
   return Promise.reject(error);
});


Vue.prototype.getParam = function (paramName) {
    var reg = new RegExp("(^|&)" + paramName + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return decodeURI(r[2]);
    } else {
        return null;
    }
}

//Vue.prototype.generateUUID = function () {
//    var d = new Date().getTime();
//    var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
//        var r = (d + Math.random() * 16) % 16 | 0;
//        d = Math.floor(d / 16);
//        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
//    });
//    return uuid;
//};
//
//Vue.prototype.dataURLtoBlob = function (dataurl) {
//    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
//        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
//    while (n--) {
//        u8arr[n] = bstr.charCodeAt(n);
//    }
//    return new Blob([u8arr], {type: mime});
//};
//
//Vue.prototype.dataURLtoFile = function (dataurl, filename) {
//    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
//        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
//    while (n--) {
//        u8arr[n] = bstr.charCodeAt(n);
//    }
//    return new File([u8arr], filename, {type: mime});
//};
//
//Vue.prototype.notice = function (message, type) {
//    type = type ? type : 'success';
//    this.$message({
//        type: type,
//        message: message
//    });
//};
//
//if (!Date.prototype.format) {
//    Date.prototype.format = function (format) {
//        var o = {
//            "M+": this.getMonth() + 1,
//            "d+": this.getDate(),
//            "h+": this.getHours(),
//            "m+": this.getMinutes(),
//            "s+": this.getSeconds(),
//            "q+": Math.floor((this.getMonth() + 3) / 3),
//            "S": this.getMilliseconds()
//        };
//        if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
//        for (var k in o) if (new RegExp("(" + k + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
//        return format;
//    }
//}

/**
 *
 * @param ossClient oss客户端
 * @param storeAs 存储路径
 * @param file file对象
 * @param cpt 需要重传的分片
 * @param options parallel并发数 partSize分片大小
 * @param pFunc 进度回调方法 返回进度
 * @param resFunc 回调方法
 */
Vue.prototype.ossMultiUpload = function (ossClient, storeAs, file, cpt, options, pFunc, resFunc) {
    var checkpoint_temp;
    var defaultOptions = {
        parallel: 10,
        partSize: 1000 * 1024
    };
    $.extend(defaultOptions, options);
    var parallel = defaultOptions.parallel, partSize = defaultOptions.partSize;
    if (cpt) {
        ossClient.multipartUpload(storeAs, file, {
            parallel: parallel,
            partSize: partSize,//设置分片大小
            timeout: 120000,//设置超时时间
            checkpoint: cpt,
            progress: function* (percent, cpt) {
                pFunc(percent);
                checkpoint_temp = cpt;
            }
        }).then(function (result) {
            resFunc(result);
        }).catch(function (error) {
            console.log(error);
            if (error.name !== 'cancel') {
                Vue.prototype.ossMultiUpload(ossClient, storeAs, file, checkpoint_temp, options, pFunc, resFunc);
            }
        });
    } else {
        ossClient.multipartUpload(storeAs, file, {
            parallel: parallel,
            partSize: partSize,//设置分片大小
            timeout: 120000,//设置超时时间
            progress: function* (percent, cpt) {
                pFunc(percent);
                checkpoint_temp = cpt;
            }
        }).then(function (result) {
            resFunc(result);
        }).catch(function (error) {
            console.log(error);
            if (error.name !== 'cancel') {
                Vue.prototype.ossMultiUpload(ossClient, storeAs, file, checkpoint_temp, options, pFunc, resFunc);
            }

        });
    }
}

/**
 *aliyun oss 临时授权
 * @returns {Promise<any>}
 */
Vue.prototype.getOssData = function () {
    return new Promise(function (resolve, reject) {
        return axiosService.post('/video/ossSts', {
            roleName: roleName
        }).then((r) => {
            if (r.data.code != 200) {
                return this.$message.error('OSS Token获取失败');
            }
            resolve(r.data.data);

        }).catch((r) => {
            reject(r);
        });
    }.bind(this));
};

/**
 * 阿里云OSS.Wrapper
 * @param data
 * @returns {OSS.Wrapper}
 */
Vue.prototype.getOssClient = function (data) {
    return new OSS.Wrapper({
        region: 'oss-cn-beijing',
        accessKeyId: data.AccessKeyId,
        accessKeySecret: data.AccessKeySecret,
        stsToken: data.SecurityToken,
        bucket: data.BucketName
    });
};

//Vue.prototype.getTencentToken = function () {
//    return axiosService.post('/file/tencentCloud-token').then(r => {
//        if (!r.data.ok) {
//            return this.$message.error('Token获取失败');
//        }
//        return r.data.sign;
//    }).catch(r => {
//        console.log(r)
//    })
//}
//
///**
// * 解析url
// */
//window.parse_url = function (url) {
//    if (!url) {
//        return [];
//    }
//    let regexp = /(\w+)\:\/\/([a-z_\-0-9]+(?:\.[a-z0-9_\-]+)+)\:?(\d*)\/([^\?]+)\??(.*)/gi,
//        result = regexp.exec(url);
//    return {
//        scheme: result[1],
//        hostname: result[2],
//        port: result[3],
//        path: result[4],
//        query: result[5]
//    };
//};
//
///**
// * 设置缓存
// * @param key 1 is sessionStorage
// * @param value
// * @param storageType
// */
//Vue.prototype.setStorage = function (key, value, storageType) {
//    if (Object.prototype.toString.call(value) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]') {
//        value = JSON.stringify(value);
//    }
//    if (!!storageType && storageType === 1) {
//
//        sessionStorage.setItem(key, value);
//    } else {
//        localStorage.setItem(key, value);
//    }
//};
//
///**
// * 获取缓存
// * @param key
// * @param storageType
// * @param isJSON
// * @returns {any}
// */
//Vue.prototype.getStorage = function (key, storageType, isJSON) {
//    let value;
//    if (!!storageType && storageType === 1) {
//        value = sessionStorage.getItem(key);
//    } else {
//        value = localStorage.getItem(key);
//    }
//    if (!!isJSON) {
//        try {
//            value = JSON.parse(value);
//        } catch (err) {
//            console.warn('value is not a JSON string =>', err);
//        }
//    }
//    return value;
//};
//
///**
// * 删除缓存
// * @param key
// * @param storageType
// */
//Vue.prototype.removeStorage = function (key, storageType) {
//    if (!!storageType && storageType === 1) {
//        sessionStorage.removeItem(key);
//    } else {
//        localStorage.removeItem(key);
//    }
//};
//
//Vue.prototype.in_array = function (needle, haystack) {
//    for (let index in haystack) {
//        if (haystack[index] == needle) {
//            return true;
//        }
//    }
//    return false;
//};
//
//
//Vue.prototype.indexOf = function (arr, val) {
//    for (let i = 0; i < arr.length; i++) {
//        if (arr[i] == val) return i;
//    }
//    return -1;
//};
//
//Vue.prototype.remove = function (arr, val) {
//    let index = this.indexOf(arr, val);
//    if (index > -1) {
//        arr.splice(index, 1);
//    }
//};
///**
// * 元素是否存在
// * @param needle
// * @param haystack
// */
//window.in_array = function (needle, haystack) {
//    for (let index in haystack) {
//        if (haystack[index] == needle) {
//            return true;
//        }
//    }
//    return false;
//};
//
//let strtotime = function (haystack, timeStamp) {
//    timeStamp = typeof timeStamp === 'undefined' ? parseInt(((new Date()).getTime()) / 1000) : parseInt(timeStamp);
//    let result = haystack.match(/^([+-])\s*(\d+)\s*(year|month|day|h|m|s)$/i);
//    let reg2 = /(\d{4})([\/-])(\d{1,2})(\2)(\d{1,2})(?:(\s*)(\d{1,2})(\:)(\d{1,2})(?:(\:)(\d{1,2}))?)?/;
//    let array = {
//        year: 365 * 86400,
//        month: 30 * 86400,
//        day: 86400,
//        hour: 3600,
//        minute: 60,
//        second: 1
//    };
//
//    if (result !== null && result[1] && result[2] && result[3]) {
//        if (result[1] === '+') {
//            return timeStamp + array[result[3]] * parseInt(result[2])
//        } else {
//            return timeStamp - array[result[3]] * parseInt(result[2])
//        }
//    } else if (reg2.test(haystack) !== null) {
//        result = haystack.match(reg2);
//        result = result.splice(1, result.length);
//        result[5] = ' ';
//        result[7] = result[9] = ':';
//        for (let i = 0, len = result.length; i < len; ++i) {
//            if (typeof result[i] === 'undefined') {
//                result[i] = '00';
//            }
//        }
//        return parseInt((new Date(result.join(''))).getTime() / 1000);
//    } else {
//        return false;
//    }
//}
